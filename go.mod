module gitlab.com/david_wright/david_wright.gitlab.io

go 1.18

require (
	github.com/hugomods/font-awesome v6.4.2+incompatible // indirect
	github.com/hugomods/icons v0.6.0 // indirect
	github.com/hugomods/icons/vendors/font-awesome v0.6.1 // indirect
	github.com/hugomods/images v0.8.3 // indirect
)
