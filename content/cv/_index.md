---
title: "CV"
date: 2024-01-02T17:28:13+01:00
draft: false
---

# David Wright { .centered }

**Software Engineering Team Lead**\
&nbsp;\
{{< icons/icon fas house >}} Zürich, Switzerland\
[{{< icons/icon fas envelope >}}](mailto::david.wright@bluewin.ch)
[{{< icons/icon fab gitlab >}}](https://gitlab.com/david_wright)
[{{< icons/icon fab github >}}](https://github.com/dav1d-wright)
[{{< icons/icon fab linkedin >}}](https://www.linkedin.com/in/david-wright-53076b95)
{ .personal-information }

- **Software Engineering Expertise:** Proven proficiency in C++, Rust, Java, Python, and other languages.
- **Specialized Skills:** Extensive experience in computer vision and machine learning inference pipelines.
- **Versatile Contributor:** Equally adept at excelling in team lead and individual contributor roles.
- **Innovation Focus:** Passionate about crafting cutting-edge solutions and staying at the forefront of technological advancements.
- **Varied Industry Experience:** Successfully navigated roles in both startup and established company environments.

---

## Professional Experience

### [PXL Vision AG](https://www.pxl-vision.com) | Zürich, CH | Oct 2021 - Present

#### Team Lead | Jun 2023 - Present

- Leading a team of software engineers responsible for the platform that powers the company’s Identity Verification solution.
  - The solution is available as a SaaS offering, as well as for on-premises and mobile devices.
- Leading development efforts for key pipeline components, encompassing:
  - Document detection, cropping, rectification, and recognition
  - Data extraction and processing
  - Text detection and recognition
  - Face detection and verification
  - Liveness verification
- Directing strategic initiatives to pursue the company's long-term vision, such as:
  - Investigated, designed, and developed a proof-of-concept for the support of decentralised digital identities (e-ID)
- Maintained an individual contributor role with a focus on making significant and high-impact technical contributions.
- Established an omni-directional mentorship structure to foster growth for team members
- Contributed to the team’s cohesion and professional development by organizing public meetups, conference attendances and post-event knowledge exchanges
  - e.g. CppCon, Meeting C++, and C++ User Group Zurich Meetup

<!--
- Strategic planning in alignment with company vision and stakeholder interests
- Fostered a culture of transparent and open communication within the technical team and the rest of the company
- Proactively identified and addressed technical challenges, minimizing team disruption
  -->

#### Technical Lead | Dec 2022 - May 2023

- Technical leadership on the team responsible for the platform that powers the company’s Identity Verification solution.
- Maintained an individual contributor role with a focus on making significant and high-impact technical contributions.

#### Software Engineer | Oct 2021 - Nov 2022 { .break-before }

- Designed and implemented several ML-based or classical CV algorithms for data extraction and security features
- Integrated native pipeline components written in C++ with diverse platforms, including WebAssembly, Python, Java, and Swift
- Independently spearheaded and executed an initiative to expose our SDK through gRPC and WebAssembly, which were successfully adopted by the company, leading to notable outcomes such as reduced server loads, improved quality of ingested data, and decreased development overhead
- Implemented a nonlinear optimization backend for unwarping of bent documents, resulting in overall better data extraction quality

<div class="centered">
    <ul class="skills-list">
      <li>C++23</li>
      <li>Python3.10</li>
      <li>Rust</li>
      <li>OpenCV</li>
      <li>ONNXRuntime</li>
      <li>Eigen</li>
      <li>Boost</li>
      <li>gRPC</li>
      <li>Protobuf</li>
      <li>Elastic Observability</li>
      <li>OpenTelemetry</li>
      <li>Boto3</li>
      <li>AWS</li>
      <li>WebAssembly</li>
      <li>PyBind11</li>
      <li>Numpy</li>
      <li>Java</li>
      <li>JNI</li>
      <li>Spring Boot</li>
      <li>GitLab-CI</li>
    </ul>
</div>

---

### [IniVation AG](https://www.inivation.com) | Zürich, CH | Feb 2020 - Sept 2021

- Design and implementation of neuromorphic machine vision algorithms
- Asynchronous event and signal processing
- High-speed monocular depth estimation and mapping
- Data clustering and segmentation
- Mathematical optimization
- Vision-based robot control
- Implementation of communication interfaces for industrial robots
- Parallel programming (OpenCL, CLBlast, boost::asio)
- Performance analysis and optimization
- 3D point cloud rendering
- BLAS

<div class="centered">
    <ul class="skills-list">
      <li>C++20</li>
      <li>Python3.8</li>
      <li>OpenCV</li>
      <li>Eigen</li>
      <li>Boost</li>
      <li>Asio</li>
      <li>FlatBuffers</li>
      <li>PostgreSQL</li>
      <li>AWS Lambda</li>
      <li>Numpy</li>
      <li>SciPy</li>
      <li>OpenCL</li>
      <li>JavaFX</li>
      <li>GitLab-CI</li>
    </ul>
</div>

---

### [Siemens AG](https://www.siemens.com) | Wallisellen, CH | Embedded Systems Engineer | Jul 2016 - Jan 2020

- Development of train protection software (C++, up to SIL4)
- Designed and evaluated algorithms for the safe determination of slip/slide behavior of trains
  - Extended Kalman Filters
  - Wavelet transform analysis
  - Statistical tests on sensor data
  - Support Vector Machines

<!--
- Software Architecture design
- Implementation of an integration test framework and integration tests for a train protection system (Ruby)
- Implementation of a user interface for production tests (C#)
- Quality Management in Projects (QMiP)
-->

<div class="centered">
    <ul class="skills-list">
      <li>C++98</li>
      <li>C#</li>
      <li>Embedded Linux</li>
      <li>GitLab-CI</li>
      <li>Groovy</li>
      <li>Ruby</li>
      <li>VHDL</li>
    </ul>
</div>

---

### [Indel AG](https://www.indel.ch) | Russikon, CH| Embedded Software Engineer | Jul 2014 - Jun 2016

- Development of firmware for functional safety supervision of industrial drives (MISRA compliant C, according to SIL3)
- Test specification and implementation (C++ and Lua)
- PCB development for I/O integration tests
<!--
- Unit testing with Tessy and CPPUnit
- Verification of redundant sensor channels
- Periodic memory and firmware checks
- Multiprocessor synchronization
- Integration of openSAFETY communication stack into the Indel real-time operating system
- Software floating-point unit library design
  -->

<div class="centered">
    <ul class="skills-list">
      <li>C++98</li>
      <li>Lua</li>
      <li>Embedded Linux</li>
      <li>GitLab-CI</li>
      <li>Groovy</li>
      <li>openSAFETY</li>
      <li>MISRA</li>
    </ul>
</div>

---

## Education { .break-before }

|                             |           |                                               |
| --------------------------- | --------- | --------------------------------------------- |
| MSc. Engineering            | 2017-2021 | HSR University of Applied Sciences Rapperswil |
| BSc. Electrical Engineering | 2011-2014 | HSR University of Applied Sciences Rapperswil |
| Apprenticeship              | 2006-2010 | Bischofberger AG Zürich: _Electrician_        |
| Vocational College          | 2006-2010 | Gewerbliche Berufsschule Wetzikon             |
| Vocational Baccalaureate    | 2006-2010 | Berufsmaturitätsschule Zürich                 |

---

## Thesises and Academic Projects

| **Type**                | **Title**                                                      | **Grade** |
| ----------------------- | -------------------------------------------------------------- | --------- |
| Master Thesis           | _Neuromorphic Vision Based 3D Laser Mapping and Robot Control_ | 6.0 / 6.0 |
| Project Thesis          | _A Reinforcement Learning Based Rover Navigation_              | 5.5 / 6.0 |
| Bachelor Thesis         | _Piezoelectric Energy Harvesting For HSRVote_                  | 6.0 / 6.0 |
| Student Research Thesis | _Optimal Sensor Calibration using Lasso Regression_            | 5.5 / 6.0 |

---

## Awards and Accolades

Appreciation Award issued by Electrosuisse AG for the Bachelor Thesis _Piezoelectric Energy Harvesting for HSRvote_

---

## Further Certificates

|        |                                                              |
| ------ | ------------------------------------------------------------ |
| CPSA-F | Certified Professional Software Architect - Foundation Level |
| PSM 1  | Professional Scrum Master 1 Certification                    |
| Tessy  | Participation at TESSY 3 training                            |
| LSO    | Laser Safety Officer                                         |

---

## Natural Language Skills

Swiss German (native), German (bilingual), English (bilingual), French (elementary)
