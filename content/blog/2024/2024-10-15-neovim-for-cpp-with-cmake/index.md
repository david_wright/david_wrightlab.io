---
title: "A Neovim Setup for C++ with CMake"
date: 2024-10-15T09:20:46+01:00
description: ""
keywords: [nvim, c++, cmake]
draft: true
---

# Introduction

This blog post will cover my Neovim setup for C++ development with CMake. I will share my key configurations, installation, and plugin management workflows that I use to make Neovim a powerful development environment for C++ projects.
The main features covered will be:

- Building CMake projects
- Debugging
- Syntax highlighting
- Linting and formatting
- Autocompletion

## Caveats

I am currently not using Neovim for any embedded projects, so my setup may not be suitable for these use cases. In particular, I do not have a setup for flashing hardware or attaching to a hardware debugger. If anyone has any suggestions for how to do this in Neovim, I would love to hear them.

# Setup

I am using Neovim primarily on Linux, inside the Kitty terminal emulator. My configuration is available on GitLab ([here](https://gitlab.com/david_wright/nvim)).

# Key Configurations

## 1. Basic Settings

Explanation of basic settings you configure.

## 2. Plugin Management

Details about the plugin manager and essential plugins you use.

## 3. Key Mappings

Custom key mappings and why you use them.

## 4. LSP and Autocompletion

Configuration for LSP (Language Server Protocol) and autocompletion setup.

# Visual Customizations

Your choice of colorschemes, statusline, and other visual enhancements.

# Conclusion

Wrap up your thoughts on your Neovim setup.
