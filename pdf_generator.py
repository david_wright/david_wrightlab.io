#!/usr/bin/env python

import asyncio
from pyppeteer import launch
import argparse
from pathlib import Path


async def main(html: Path, pdf: Path, additional_js: Path = None):
    browser = await launch(options={
        'headless':
        True,
        'args': [
            '--no-sandbox',
            '--disable-setuid-sandbox',
        ],
    }, )
    page = await browser.newPage()
    await page.goto('file://' + str(html.resolve().absolute()))
    await page.pdf({'path': pdf, 'printBackground': True})
    if additional_js:
        await page.evaluate(open(additional_js).read())
    await browser.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--html', type=Path)
    parser.add_argument('--pdf', type=Path)
    parser.add_argument('--additional_js', type=Path)
    args = parser.parse_args()

    asyncio.get_event_loop().run_until_complete(
        main(args.html, args.pdf, args.additional_js))
