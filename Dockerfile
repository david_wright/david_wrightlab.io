FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
ENV BASH_ENV="/root/.bashrc"

SHELL ["/bin/bash", "-l", "-c"]

RUN apt update &&  apt install -y \
      brotli \
      sudo \
      npm \
      curl \
      git \
      gcc \
      g++ \
      ca-certificates \
      fonts-liberation \
      libappindicator3-1 \
      libasound2 \
      libatk-bridge2.0-0 \
      libatk1.0-0 \
      libc6 \
      libcairo2 \
      libcups2 \
      libdbus-1-3 \
      libexpat1 \
      libfontconfig1 \
      libgbm1 \
      libgcc1 \
      libglib2.0-0 \
      libgtk-3-0 \
      libnspr4 \
      libnss3 \
      libpango-1.0-0 \
      libpangocairo-1.0-0 \
      libstdc++6 \
      libx11-6 \
      libx11-xcb1 \
      libxcb1 \
      libxcomposite1 \
      libxcursor1 \
      libxdamage1 \
      libxext6 \
      libxfixes3 \
      libxi6 \
      libxrandr2 \
      libxrender1 \
      libxss1 \
      libxtst6 \
      lsb-release \
      wget \
      xdg-utils \
      python3 \
      python-is-python3 \
      python3-pip

COPY requirements.txt /tmp/requirements.txt

RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash \
    && export NVM_DIR="$HOME/.nvm" && [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" \
    && nvm install 20.5.0 \
    && nvm use 20.5.0 \
    && npm install -g sass \
    && wget https://golang.org/dl/go1.21.5.linux-amd64.tar.gz \
    && rm -rf /usr/local/go && tar -C /usr/local -xzf go1.21.5.linux-amd64.tar.gz \
    && rm -rf go1.21.5.linux-amd64.tar.gz \
    && echo 'export PATH=$PATH:/usr/local/go/bin' >> /root/.bashrc \
    && export PATH=$PATH:/usr/local/go/bin \
    && wget https://github.com/gohugoio/hugo/releases/download/v0.121.1/hugo_0.121.1_linux-amd64.deb \
    && sudo apt install ./hugo_0.121.1_linux-amd64.deb  \
    && rm -rf hugo_0.121.1_linux-amd64.deb \
    && python -m pip install --upgrade pip \
    && pip install -r /tmp/requirements.txt 

ARG WORKSPACE=/docker_volumes
WORKDIR $WORKSPACE

ENTRYPOINT ${SHELL}
